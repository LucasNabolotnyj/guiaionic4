import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  pages: Array<{icone:string, titulo: string, url: any}>;
  private mobi=false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    // variavel para o ngFor e navegacao do menu lateral
    this.pages = 
    [
      { icone:'assets/icon/icone_aunipampa.svg', titulo: 'A Universidade', url:'/a-universidade'},
      { icone:'assets/icon/icone_calendario_academico_cor.svg', titulo: 'Calendário Acadêmico', url:'/calendario-acad'},
      { icone:'assets/icon/icone_assistencia_estudantil_cor.svg', titulo: 'Assistência Estudantil', url:'/assistencia-estudantil'},
      { icone:'assets/icon/icone_participacao_estudantil_cor.svg', titulo: 'Participação Estudantil', url:'/participacao-estudantil'},
      { icone:'assets/icon/icone_atividades_academicas_cor.svg', titulo: 'Atividades Acadêmcias', url:'/atividades-acad'},
      { icone:'assets/icon/icone_estagio_cor.svg', titulo: 'Estágios', url:'/estagios'},
      { icone:'assets/icon/icone_intercambio_cor.svg', titulo: 'Intercâmbios', url:'/intercambios'},
      { icone:'assets/icon/icone_servicos_online_cor.svg', titulo: 'Serviços Online', url:'/servicos-online'},
      { icone:'assets/icon/icone_contatos_cor.svg', titulo: 'Contatos', url:'/contatos'},
      { icone:'assets/icon/icone_dicas_cor.svg', titulo: 'Dicas', url:'/dicas'}
    ];
    if(!this.platform.is('mobile')){
      this.mobi=true;
      // this.pages.push({icone:'assets/icon/ic_home.svg', titulo:'Login', url:'/login'});
    }
  }

  initializeApp() {
    
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    
    
  }
}
