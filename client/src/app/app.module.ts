import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatabaseService } from './database/database.service';
import { HttpClientModule } from '@angular/common/http';

import { PopoverController } from '@ionic/angular';
import { PopoverContent } from './pages/tabs/tabs.page';
import { ReactiveFormsModule} from '@angular/forms';
@NgModule({
  declarations: [AppComponent, PopoverContent],
  entryComponents: [PopoverContent],
  imports: [BrowserModule,
     IonicModule.forRoot(),
     AppRoutingModule,
     HttpClientModule,                     
     ReactiveFormsModule      
    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    DatabaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
