import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { isUndefined } from 'util';
@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private jwt: any;
  constructor(private httpClient: HttpClient) { }
  getStringTudo(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/');
  }
  getStringAUni(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9362');
  }
  getStringEv(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9365');
  }
  getStringEdit(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9366');
  }
  getStringAssistE(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9367');
  }
  getStringServOnl(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9368');
  }
  getStringPartEst(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9369');
  }
  getStringAtivAcad(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd936a');
  }
  getStringEst(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd936b');
  }
  getStringInterc(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd936c');
  }
  getStringContatos(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd936d');
  }
  getStringDicas(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd936f');
  }
  getStringSobre(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9370');
  }
  getStringFeedback(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/strings/5dde7b008f81a98093fd9371');
  }

  getNoticias(): Observable<any> {
    return this.httpClient.get('http://localhost:8888/feed-rss');
  }

  alteraString(Id: string, content: string, page: string): boolean {
    // prepara corpo da requisicao HTTP e envia a requisicao via POST
    let body = '{ "page":' + page + ', "content":' + content + '}';
    let url = 'http://localhost:8888/strings/' + Id;
    if (isUndefined(this.jwt)) {
      console.log("Chave JWT não autenticada.");
      return true;
    }
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + this.jwt.token);
    // let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.jwt.token);
    this.httpClient.patch(url, body, { headers: headers }).subscribe((res) => {
      console.log("Valores Atualizados com sucesso!  " + JSON.stringify(res));
      return false;
    },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('Erro Client-side Encontrado.');
        } else {
          console.log('Erro Server-side Encontrado.');
        }
        return true;
      })
    return false;
  }

  signIn(usuario: any): Observable<boolean> {
    let url = 'http://localhost:8888/auth';
    const body = {
      email: usuario.email, senha: usuario.senha
    };
    const headers = new HttpHeaders();
    headers.set('Content-Type:', 'text/plain; charset=utf-8');

    return this.httpClient.post(url, body, { headers: headers }).pipe(map(resposta => {
      if (resposta) {
        this.jwt = resposta;
        return true;
      } else {
        return false
      }
    }));

  }
  
//   signIn(usuario: any):boolean {
//     let temp=false;
//     let url = 'http://localhost:8888/auth';
//     const body = {
//       email: usuario.email, senha: usuario.senha
//     };
//     const headers = new HttpHeaders();
//     headers.set('Content-Type:', 'text/plain; charset=utf-8');

//    this.httpClient.post(url, body, { headers: headers }).subscribe((res) => {
//       // console.log("loguei  " + JSON.stringify(res));
//       this.jwt = res;
//       temp= false;
//     },
//       (err: HttpErrorResponse) => {
//         if (err.error instanceof Error) {
//           console.log('Erro Client-side Encontrado.');
//         } else {
//           console.log('Erro Server-side Encontrado.');
//         }
//         temp= true;
//       });
// return temp;
//   }

  logout() {
    this.jwt = null;
  }


}
