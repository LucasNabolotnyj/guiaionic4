import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AUniversidadePage } from './a-universidade.page';

describe('AUniversidadePage', () => {
  let component: AUniversidadePage;
  let fixture: ComponentFixture<AUniversidadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AUniversidadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AUniversidadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
