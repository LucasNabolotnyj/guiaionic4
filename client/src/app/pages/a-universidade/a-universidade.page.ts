import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-a-universidade',
  templateUrl: './a-universidade.page.html',
  styleUrls: ['./a-universidade.page.scss'],
})
export class AUniversidadePage implements OnInit {
private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringAUni().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}
