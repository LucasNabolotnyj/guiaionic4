import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ApoioIngressantePage } from './apoio-ingressante.page';

const routes: Routes = [
  {
    path: '',
    component: ApoioIngressantePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ApoioIngressantePage]
})
export class ApoioIngressantePageModule {}
