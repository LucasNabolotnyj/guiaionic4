import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApoioIngressantePage } from './apoio-ingressante.page';

describe('ApoioIngressantePage', () => {
  let component: ApoioIngressantePage;
  let fixture: ComponentFixture<ApoioIngressantePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApoioIngressantePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApoioIngressantePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
