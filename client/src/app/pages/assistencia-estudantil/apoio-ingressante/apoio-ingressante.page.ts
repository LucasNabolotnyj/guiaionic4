import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-apoio-ingressante',
  templateUrl: './apoio-ingressante.page.html',
  styleUrls: ['./apoio-ingressante.page.scss'],
})
export class ApoioIngressantePage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}