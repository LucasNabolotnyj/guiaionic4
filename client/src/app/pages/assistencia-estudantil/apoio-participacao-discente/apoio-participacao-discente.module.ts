import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ApoioParticipacaoDiscentePage } from './apoio-participacao-discente.page';

const routes: Routes = [
  {
    path: '',
    component: ApoioParticipacaoDiscentePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ApoioParticipacaoDiscentePage]
})
export class ApoioParticipacaoDiscentePageModule {}
