import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-apoio-participacao-discente',
  templateUrl: './apoio-participacao-discente.page.html',
  styleUrls: ['./apoio-participacao-discente.page.scss'],
})
export class ApoioParticipacaoDiscentePage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}