import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApoioSocialPedagogicoPage } from './apoio-social-pedagogico.page';

describe('ApoioSocialPedagogicoPage', () => {
  let component: ApoioSocialPedagogicoPage;
  let fixture: ComponentFixture<ApoioSocialPedagogicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApoioSocialPedagogicoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApoioSocialPedagogicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
