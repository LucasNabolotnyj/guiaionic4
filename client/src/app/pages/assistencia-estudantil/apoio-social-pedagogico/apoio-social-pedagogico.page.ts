import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-apoio-social-pedagogico',
  templateUrl: './apoio-social-pedagogico.page.html',
  styleUrls: ['./apoio-social-pedagogico.page.scss'],
})
export class ApoioSocialPedagogicoPage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}