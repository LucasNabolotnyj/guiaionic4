import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';
import { NavParams, NavController } from '@ionic/angular';

@Component({
  selector: 'app-assistencia-estudantil',
  templateUrl: './assistencia-estudantil.page.html',
  styleUrls: ['./assistencia-estudantil.page.scss'],
})
export class AssistenciaEstudantilPage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}