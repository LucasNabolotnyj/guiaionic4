import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConcorrerBeneficiosPage } from './concorrer-beneficios.page';

const routes: Routes = [
  {
    path: '',
    component: ConcorrerBeneficiosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConcorrerBeneficiosPage]
})
export class ConcorrerBeneficiosPageModule {}
