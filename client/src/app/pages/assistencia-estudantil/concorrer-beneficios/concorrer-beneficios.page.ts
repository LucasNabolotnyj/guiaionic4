import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-concorrer-beneficios',
  templateUrl: './concorrer-beneficios.page.html',
  styleUrls: ['./concorrer-beneficios.page.scss'],
})
export class ConcorrerBeneficiosPage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}