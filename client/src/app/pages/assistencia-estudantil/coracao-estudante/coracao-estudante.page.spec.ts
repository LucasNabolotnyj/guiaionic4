import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoracaoEstudantePage } from './coracao-estudante.page';

describe('CoracaoEstudantePage', () => {
  let component: CoracaoEstudantePage;
  let fixture: ComponentFixture<CoracaoEstudantePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoracaoEstudantePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoracaoEstudantePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
