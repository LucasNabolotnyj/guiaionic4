import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-coracao-estudante',
  templateUrl: './coracao-estudante.page.html',
  styleUrls: ['./coracao-estudante.page.scss'],
})
export class CoracaoEstudantePage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}