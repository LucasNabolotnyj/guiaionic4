import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CulturaEspLazIncPage } from './cultura-esp-laz-inc.page';

describe('CulturaEspLazIncPage', () => {
  let component: CulturaEspLazIncPage;
  let fixture: ComponentFixture<CulturaEspLazIncPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CulturaEspLazIncPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CulturaEspLazIncPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
