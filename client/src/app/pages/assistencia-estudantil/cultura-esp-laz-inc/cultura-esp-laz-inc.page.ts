import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-cultura-esp-laz-inc',
  templateUrl: './cultura-esp-laz-inc.page.html',
  styleUrls: ['./cultura-esp-laz-inc.page.scss'],
})
export class CulturaEspLazIncPage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}