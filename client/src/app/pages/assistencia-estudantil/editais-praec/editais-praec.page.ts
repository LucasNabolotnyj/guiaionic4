import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';
@Component({
  selector: 'app-editais-praec',
  templateUrl: './editais-praec.page.html',
  styleUrls: ['./editais-praec.page.scss'],
})
export class EditaisPraecPage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService
    ) {
  }
  
 async ngOnInit() {
    // try {
    //   await this.dbService.getStringAssistE().subscribe((res) => { 
    //     console.log(res);
    //     this.retornos = res });
    // } catch (err) {
    //   console.log(err);
    // }
    alert("Esta página carrega um site externo e está em processo de mudança");
    window.open("http://www.guiadoalunounipampa.com/editais/index.html");
  }
}