import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanoPermanenciaPage } from './plano-permanencia.page';

describe('PlanoPermanenciaPage', () => {
  let component: PlanoPermanenciaPage;
  let fixture: ComponentFixture<PlanoPermanenciaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanoPermanenciaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanoPermanenciaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
