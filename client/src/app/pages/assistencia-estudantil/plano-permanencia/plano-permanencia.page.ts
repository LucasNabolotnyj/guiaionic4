import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-plano-permanencia',
  templateUrl: './plano-permanencia.page.html',
  styleUrls: ['./plano-permanencia.page.scss'],
})
export class PlanoPermanenciaPage implements OnInit {
  public retornos: any;

  constructor(private dbService: DatabaseService) {
  }
  
 async ngOnInit() {
    try {
      await this.dbService.getStringAssistE().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}