import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AtividadesAcadPage } from './atividades-acad.page';

const routes: Routes = [
  {
    path: '',
    component: AtividadesAcadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AtividadesAcadPage]
})
export class AtividadesAcadPageModule {}
