import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtensaoCulturaPage } from './extensao-cultura.page';

describe('ExtensaoCulturaPage', () => {
  let component: ExtensaoCulturaPage;
  let fixture: ComponentFixture<ExtensaoCulturaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtensaoCulturaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtensaoCulturaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
