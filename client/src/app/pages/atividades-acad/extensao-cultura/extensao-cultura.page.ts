import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-extensao-cultura',
  templateUrl: './extensao-cultura.page.html',
  styleUrls: ['./extensao-cultura.page.scss'],
})
export class ExtensaoCulturaPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringAtivAcad().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}