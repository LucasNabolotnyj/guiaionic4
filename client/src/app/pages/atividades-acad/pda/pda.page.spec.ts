import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdaPage } from './pda.page';

describe('PdaPage', () => {
  let component: PdaPage;
  let fixture: ComponentFixture<PdaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
