import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampiPage } from './campi.page';

describe('CampiPage', () => {
  let component: CampiPage;
  let fixture: ComponentFixture<CampiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
