import { Component, OnInit, ChangeDetectorRef, QueryList } from '@angular/core';
import { NavController } from '@ionic/angular';

import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-campi',
  templateUrl: './campi.page.html',
  styleUrls: ['./campi.page.scss'],
})
export class CampiPage implements OnInit {
  private retornos: any;
  private lista: any;
  constructor(private dbService: DatabaseService,
    private changeDetector: ChangeDetectorRef,
    private navControl: NavController) {
    this.lista = [
      { open: false },
      { open: false },
      { open: false },
      { open: false },
      { open: false },
      { open: false },
      { open: false },
      { open: false },
      { open: false },
      { open: false }
    ]
  }
  navega(url) {
    this.navControl.navigateForward(url);
  }

  setOpen(i) {
    // this.lista.forEach(j => {
    //   j.open = false;
    // });
    this.lista[i].open = !this.lista[i].open;
    this.changeDetector.detectChanges();
    console.log(this.lista[i].open);

  }
  getOpen(i): boolean {
    return this.lista[i].open;
  }

  async ngOnInit() {
    try {
      await this.dbService.getStringContatos().subscribe((res) => {
        console.log(res);
        this.retornos = res
      });
    } catch (err) {
      console.log(err);
    }
  }
}