import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComoAcharSiteCursoPage } from './como-achar-site-curso.page';

const routes: Routes = [
  {
    path: '',
    component: ComoAcharSiteCursoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ComoAcharSiteCursoPage]
})
export class ComoAcharSiteCursoPageModule {}
