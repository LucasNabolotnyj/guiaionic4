import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComoAcharSiteCursoPage } from './como-achar-site-curso.page';

describe('ComoAcharSiteCursoPage', () => {
  let component: ComoAcharSiteCursoPage;
  let fixture: ComponentFixture<ComoAcharSiteCursoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComoAcharSiteCursoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComoAcharSiteCursoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
