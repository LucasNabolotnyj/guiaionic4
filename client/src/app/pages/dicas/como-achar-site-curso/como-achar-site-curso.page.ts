import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-como-achar-site-curso',
  templateUrl: './como-achar-site-curso.page.html',
  styleUrls: ['./como-achar-site-curso.page.scss'],
})
export class ComoAcharSiteCursoPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringDicas().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}