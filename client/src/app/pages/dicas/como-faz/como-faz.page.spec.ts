import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComoFazPage } from './como-faz.page';

describe('ComoFazPage', () => {
  let component: ComoFazPage;
  let fixture: ComponentFixture<ComoFazPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComoFazPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComoFazPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
