import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-criterios-aprovacao',
  templateUrl: './criterios-aprovacao.page.html',
  styleUrls: ['./criterios-aprovacao.page.scss'],
})
export class CriteriosAprovacaoPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringDicas().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}