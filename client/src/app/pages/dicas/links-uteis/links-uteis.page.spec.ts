import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinksUteisPage } from './links-uteis.page';

describe('LinksUteisPage', () => {
  let component: LinksUteisPage;
  let fixture: ComponentFixture<LinksUteisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinksUteisPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinksUteisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
