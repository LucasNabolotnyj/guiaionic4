import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginInstitucionalPage } from './login-institucional.page';

const routes: Routes = [
  {
    path: '',
    component: LoginInstitucionalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginInstitucionalPage]
})
export class LoginInstitucionalPageModule {}
