import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-edicao',
  templateUrl: './edicao.page.html',
  styleUrls: ['./edicao.page.scss'],
})
export class EdicaoPage implements OnInit {
  private retornos:any;
  private pagina:any;
  private doc_id:any;
  private conteudo:any;
  constructor(
    private dbservice:DatabaseService
      ) {  
  }

  
  receberValor(C:any){
    this.doc_id = C.detail.value._id
    this.conteudo = this.getValor(C.detail.value.content);
    this.pagina = this.getValor(C.detail.value.page);
  }
getValor(rt:any):String{
  return JSON.stringify(rt);
}

registrar(){
  if(this.dbservice.alteraString(this.doc_id,this.conteudo,this.pagina)){
    alert("Ação recusada pelo servidor. Verifique sua conexão e efetue o login novamente");
  }
  // console.log(this.EditForm.value);
}
limpaForm(){
  this.conteudo = '';
  this.doc_id = '';
}
 async ngOnInit() {
    try {
      await this.dbservice
      .getStringTudo().subscribe((res) => { 
        this.retornos = res;
        // console.log(this.retornos); 
      });
    } catch (err) {
      console.log(err);
    }
  }
}
