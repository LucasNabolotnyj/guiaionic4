import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstagiosPage } from './estagios.page';

describe('EstagiosPage', () => {
  let component: EstagiosPage;
  let fixture: ComponentFixture<EstagiosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstagiosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstagiosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
