import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-bracol',
  templateUrl: './bracol.page.html',
  styleUrls: ['./bracol.page.scss'],
})
export class BracolPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringInterc().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}
