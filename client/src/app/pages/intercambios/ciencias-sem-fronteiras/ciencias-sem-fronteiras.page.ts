import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-ciencias-sem-fronteiras',
  templateUrl: './ciencias-sem-fronteiras.page.html',
  styleUrls: ['./ciencias-sem-fronteiras.page.scss'],
})
export class CienciasSemFronteirasPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringInterc().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}