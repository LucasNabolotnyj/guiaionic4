import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-experiencias-intercambios',
  templateUrl: './experiencias-intercambios.page.html',
  styleUrls: ['./experiencias-intercambios.page.scss'],
})
export class ExperienciasIntercambiosPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringInterc().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}