import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdiomasSemFronteirasPage } from './idiomas-sem-fronteiras.page';

describe('IdiomasSemFronteirasPage', () => {
  let component: IdiomasSemFronteirasPage;
  let fixture: ComponentFixture<IdiomasSemFronteirasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdiomasSemFronteirasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdiomasSemFronteirasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
