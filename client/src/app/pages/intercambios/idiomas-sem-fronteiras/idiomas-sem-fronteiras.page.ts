import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-idiomas-sem-fronteiras',
  templateUrl: './idiomas-sem-fronteiras.page.html',
  styleUrls: ['./idiomas-sem-fronteiras.page.scss'],
})
export class IdiomasSemFronteirasPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringInterc().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}