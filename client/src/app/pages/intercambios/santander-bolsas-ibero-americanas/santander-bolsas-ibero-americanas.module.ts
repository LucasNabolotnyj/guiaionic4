import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SantanderBolsasIberoAmericanasPage } from './santander-bolsas-ibero-americanas.page';

const routes: Routes = [
  {
    path: '',
    component: SantanderBolsasIberoAmericanasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SantanderBolsasIberoAmericanasPage]
})
export class SantanderBolsasIberoAmericanasPageModule {}
