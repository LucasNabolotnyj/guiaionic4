import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SantanderBolsasIberoAmericanasPage } from './santander-bolsas-ibero-americanas.page';

describe('SantanderBolsasIberoAmericanasPage', () => {
  let component: SantanderBolsasIberoAmericanasPage;
  let fixture: ComponentFixture<SantanderBolsasIberoAmericanasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SantanderBolsasIberoAmericanasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SantanderBolsasIberoAmericanasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
