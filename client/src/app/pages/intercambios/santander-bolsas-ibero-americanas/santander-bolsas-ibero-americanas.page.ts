import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-santander-bolsas-ibero-americanas',
  templateUrl: './santander-bolsas-ibero-americanas.page.html',
  styleUrls: ['./santander-bolsas-ibero-americanas.page.scss'],
})
export class SantanderBolsasIberoAmericanasPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringInterc().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}