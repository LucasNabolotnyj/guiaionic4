import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DatabaseService } from 'src/app/database/database.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  constructor(public dbservice: DatabaseService,
    public formBuilder: FormBuilder,
    public router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern(
        '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
      Validators.required])],
      senha: ['', Validators.required]
    });
  }
  async fazLogin() {
    if (this.loginForm.invalid) return;

    await this.dbservice.signIn(this.loginForm.value).subscribe((res) => {
      if (res) {
        this.router.navigateByUrl('/edicao');
        return;
      }
    }, (err) => {
      alert("Solicitação recusada, verifque sua conexão e credenciais.");
      this.router.navigateByUrl('/login');
    }
    );


    //  let val = await this.dbservice.SignIn(this.loginForm.value);


  }
  ngOnInit() {
  }
}
