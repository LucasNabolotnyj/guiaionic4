import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-comissao-campi',
  templateUrl: './comissao-campi.page.html',
  styleUrls: ['./comissao-campi.page.scss'],
})
export class ComissaoCampiPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringPartEst().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}