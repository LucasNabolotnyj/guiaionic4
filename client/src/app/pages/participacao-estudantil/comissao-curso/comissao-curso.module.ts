import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComissaoCursoPage } from './comissao-curso.page';

const routes: Routes = [
  {
    path: '',
    component: ComissaoCursoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ComissaoCursoPage]
})
export class ComissaoCursoPageModule {}
