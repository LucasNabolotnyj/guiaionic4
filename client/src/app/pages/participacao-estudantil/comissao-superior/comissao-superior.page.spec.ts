import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissaoSuperiorPage } from './comissao-superior.page';

describe('ComissaoSuperiorPage', () => {
  let component: ComissaoSuperiorPage;
  let fixture: ComponentFixture<ComissaoSuperiorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissaoSuperiorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissaoSuperiorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
