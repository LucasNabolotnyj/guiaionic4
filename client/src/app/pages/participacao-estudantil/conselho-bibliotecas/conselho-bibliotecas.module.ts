import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConselhoBibliotecasPage } from './conselho-bibliotecas.page';

const routes: Routes = [
  {
    path: '',
    component: ConselhoBibliotecasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConselhoBibliotecasPage]
})
export class ConselhoBibliotecasPageModule {}
