import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConselhoBibliotecasPage } from './conselho-bibliotecas.page';

describe('ConselhoBibliotecasPage', () => {
  let component: ConselhoBibliotecasPage;
  let fixture: ComponentFixture<ConselhoBibliotecasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConselhoBibliotecasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConselhoBibliotecasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
