import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-conselho-bibliotecas',
  templateUrl: './conselho-bibliotecas.page.html',
  styleUrls: ['./conselho-bibliotecas.page.scss'],
})
export class ConselhoBibliotecasPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringPartEst().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}