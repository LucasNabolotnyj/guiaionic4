import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConselhoCuradorPage } from './conselho-curador.page';

const routes: Routes = [
  {
    path: '',
    component: ConselhoCuradorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConselhoCuradorPage]
})
export class ConselhoCuradorPageModule {}
