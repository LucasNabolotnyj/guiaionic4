import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConselhoCuradorPage } from './conselho-curador.page';

describe('ConselhoCuradorPage', () => {
  let component: ConselhoCuradorPage;
  let fixture: ComponentFixture<ConselhoCuradorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConselhoCuradorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConselhoCuradorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
