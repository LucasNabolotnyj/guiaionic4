import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConselhoDeCampusPage } from './conselho-de-campus.page';

const routes: Routes = [
  {
    path: '',
    component: ConselhoDeCampusPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConselhoDeCampusPage]
})
export class ConselhoDeCampusPageModule {}
