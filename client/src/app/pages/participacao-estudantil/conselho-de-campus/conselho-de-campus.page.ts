import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-conselho-de-campus',
  templateUrl: './conselho-de-campus.page.html',
  styleUrls: ['./conselho-de-campus.page.scss'],
})
export class ConselhoDeCampusPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringPartEst().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}