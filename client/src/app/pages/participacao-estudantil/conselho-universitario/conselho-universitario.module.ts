import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConselhoUniversitarioPage } from './conselho-universitario.page';

const routes: Routes = [
  {
    path: '',
    component: ConselhoUniversitarioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConselhoUniversitarioPage]
})
export class ConselhoUniversitarioPageModule {}
