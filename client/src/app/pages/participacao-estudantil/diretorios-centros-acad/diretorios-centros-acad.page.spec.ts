import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiretoriosCentrosAcadPage } from './diretorios-centros-acad.page';

describe('DiretoriosCentrosAcadPage', () => {
  let component: DiretoriosCentrosAcadPage;
  let fixture: ComponentFixture<DiretoriosCentrosAcadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiretoriosCentrosAcadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiretoriosCentrosAcadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
