import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-diretorios-centros-acad',
  templateUrl: './diretorios-centros-acad.page.html',
  styleUrls: ['./diretorios-centros-acad.page.scss'],
})
export class DiretoriosCentrosAcadPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringPartEst().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}