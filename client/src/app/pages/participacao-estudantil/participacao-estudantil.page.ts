import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-participacao-estudantil',
  templateUrl: './participacao-estudantil.page.html',
  styleUrls: ['./participacao-estudantil.page.scss'],
})
export class ParticipacaoEstudantilPage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringPartEst().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}