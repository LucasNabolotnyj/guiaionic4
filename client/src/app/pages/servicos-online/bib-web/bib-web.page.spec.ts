import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibWebPage } from './bib-web.page';

describe('BibWebPage', () => {
  let component: BibWebPage;
  let fixture: ComponentFixture<BibWebPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibWebPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibWebPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
