import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GauchaPage } from './gaucha.page';

describe('GauchaPage', () => {
  let component: GauchaPage;
  let fixture: ComponentFixture<GauchaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GauchaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GauchaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
