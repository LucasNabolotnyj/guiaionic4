import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuriPage } from './guri.page';

describe('GuriPage', () => {
  let component: GuriPage;
  let fixture: ComponentFixture<GuriPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuriPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuriPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
