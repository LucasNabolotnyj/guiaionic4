import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalAlunoPage } from './portal-aluno.page';

describe('PortalAlunoPage', () => {
  let component: PortalAlunoPage;
  let fixture: ComponentFixture<PortalAlunoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortalAlunoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalAlunoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
