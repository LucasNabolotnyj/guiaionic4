import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicosOnlinePage } from './servicos-online.page';

describe('ServicosOnlinePage', () => {
  let component: ServicosOnlinePage;
  let fixture: ComponentFixture<ServicosOnlinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicosOnlinePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicosOnlinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
