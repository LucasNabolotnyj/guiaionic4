import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.page.html',
  styleUrls: ['./sobre.page.scss'],
})
export class SobrePage implements OnInit {
  private retornos:any;
  constructor(private dbService: DatabaseService) { }

 async ngOnInit() {
    try {
      await this.dbService.getStringSobre().subscribe((res) => { 
        console.log(res);
        this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }
}
