import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  constructor( 
    private popoverController:PopoverController
    ) {}

  async presentPopover(ev: any) { 
    const popover = await this.popoverController.create({
      component: PopoverContent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
}


@Component({
  template: `
  <ion-content>
  <ion-list>
    <ion-item [routerDirection]="'forward'" [routerLink]="['/feedback']" class="ion-button" (click)="disablePopover($event)">
    <ion-label>
      Feedback
    </ion-label>
    </ion-item>
    <ion-item [routerDirection]="'forward'" [routerLink]="['/sobre']" lines="none" class="ion-button" (click)="disablePopover($event)">
    <ion-label>
    Sobre
    </ion-label>
    </ion-item>
  </ion-list>
  </ion-content>
`
})
export class PopoverContent {
  constructor( 
    private popoverController:PopoverController
    ) {}
    
  async disablePopover(ev: any) {
    return await this.popoverController.dismiss();
} 

}
