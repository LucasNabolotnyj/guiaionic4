import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StringsModule } from './strings/strings.module';
// import { UsuariosModule } from './usuarios/usuarios.module';
import { FeedRssController } from './feed-rss/feed-rss.controller';
import { FeedRssService } from './feed-rss/feed-rss.service';
import { FeedRssModule } from './feed-rss/feed-rss.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { AuthGuard } from './auth/guards/auth.guard';


@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/gda'),
   StringsModule,
   UsersModule,
   FeedRssModule,
   AuthModule
  ],
  controllers: [AppController, FeedRssController],
  providers: [AppService, FeedRssService],
})
export class AppModule {}
