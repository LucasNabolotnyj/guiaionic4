import { Controller, Post, Body, InternalServerErrorException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsuarioLoginDto } from '../users/dto/login-user.dto'

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {

    }

    @Post() 
    async login(@Body() loginUsuarioDto: UsuarioLoginDto){
         console.log(loginUsuarioDto.email);
        return await this.authService.validaUsuario(loginUsuarioDto);
    }

}