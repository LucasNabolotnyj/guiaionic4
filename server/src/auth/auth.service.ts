import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsuarioLoginDto } from '../users/dto/login-user.dto';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {

    constructor(private usuarioService: UsersService, private jwtService: JwtService){

    }

    async validaUsuario(tentativaLogin: UsuarioLoginDto) {
        // Busca por login inicial
        let usuarioEncontrado = await this.usuarioService.findOneByEmail(tentativaLogin.email);
      if(usuarioEncontrado==null) throw new UnauthorizedException();
        return new Promise((resolve) => {

            // Checa senha da tentativa com a senha cadastrada no email
            usuarioEncontrado.checaSenha(tentativaLogin.senha, (err, isMatch) => {
    
                if(err) throw new UnauthorizedException();
    
                if(isMatch){
                    // Se encontrado, gera JWT para o usuario
                    resolve(this.criaPayloadJwt(usuarioEncontrado));
    
                } else {
                    throw new UnauthorizedException();
                }
    
            });

        });

    }

    async validaUsuarioPorJwt(payload: JwtPayload) { 
        // Usado quanto o usuario jah logou e tem um JWT
        let usuario = await this.usuarioService.findOneByEmail(payload.email);
        if(usuario){
            return this.criaPayloadJwt(usuario);
        } else {
            throw new UnauthorizedException();
        }
    }
    //não é necessário por causa do framework
    // async verificaToken (bearer:any) { 
    //     // Usado quanto o usuario jah logou e tem um JWT
    //     let usuario = await this.usuarioService.findOneByEmail(bearer.email);
    //     if(usuario){
    //         // if(this.jwtService.verify(bearer.token))return true;
    //         return true;
    //     } else {
    //         throw new UnauthorizedException();
    //     }
    // }

    criaPayloadJwt(usuario){
        let data: JwtPayload = {
            email: usuario.email
        };
        let jwt = this.jwtService.sign(data);
        return {
            expiresIn: 320,
            token: jwt            
        }
    }
}
