import { Test, TestingModule } from '@nestjs/testing';
import { FeedRssController } from './feed-rss.controller';

describe('FeedRss Controller', () => {
  let controller: FeedRssController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FeedRssController],
    }).compile();

    controller = module.get<FeedRssController>(FeedRssController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
