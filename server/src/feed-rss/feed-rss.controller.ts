import { Controller, Get, Res, HttpStatus } from '@nestjs/common';
import { FeedRssService } from './feed-rss.service';

@Controller('feed-rss')
export class FeedRssController {
    constructor(private readonly FeedRssService: FeedRssService) { }
    @Get()
    async getAllStringDB(@Res() res) {
        const feedNoticias = await this.FeedRssService.getRSSFeed();
        return res.status(HttpStatus.OK).json(feedNoticias);
    }
}
