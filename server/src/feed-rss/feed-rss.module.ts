import { Module } from '@nestjs/common';
import { FeedRssController } from './feed-rss.controller';
import { FeedRssService } from './feed-rss.service';

@Module({
    imports: [
    ],
    controllers: [FeedRssController],
    providers: [FeedRssService]
})
  
export class FeedRssModule {}
