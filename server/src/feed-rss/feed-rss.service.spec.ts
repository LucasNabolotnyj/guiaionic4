import { Test, TestingModule } from '@nestjs/testing';
import { FeedRssService } from './feed-rss.service';

describe('FeedRssService', () => {
  let service: FeedRssService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FeedRssService],
    }).compile();

    service = module.get<FeedRssService>(FeedRssService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
