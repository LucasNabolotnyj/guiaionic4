import { Injectable } from '@nestjs/common';

@Injectable()
export class FeedRssService {

    async getRSSFeed() {
        let Parser = require('rss-parser');
        let parser = new Parser();
        let feed = await parser.parseURL('http://www.unipampa.edu.br/portal/feed-noticias');
        return feed;
    }
}
