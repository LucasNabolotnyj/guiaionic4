import { Controller, Post, Get, Res, Body, HttpStatus, Param, NotFoundException, Patch, UseGuards } from '@nestjs/common';
import { StringsService } from './strings.service';
import { StringsDTO } from './strings.dto';
import { async } from 'rxjs/internal/scheduler/async';
import { AuthGuard } from '@nestjs/passport';
import { isUndefined } from 'util';

@Controller('strings')
export class StringsController {
    constructor(private readonly DatabaseService: StringsService) { }

    // adiciona uma String
    // @Post()
    // async addString(@Res() res, @Body() AssitenciaEDTO: StringsDTO) {
    //     const StringDB = await this.DatabaseService.addString(AssitenciaEDTO);
    //     return res.status(HttpStatus.OK).json({
    //         message: "String Criada Com Sucesso",
    //         StringDB
    //     })
    // }

    @UseGuards(AuthGuard('jwt'))
    @Patch(':StringDBID')
    async updateString(@Res() res, @Param('StringDBID') StringDBID, @Body() body: StringsDTO) {
        const StringDB = await this.DatabaseService.updateString(StringDBID, body);
        // console.log(StringDB);
        if (isUndefined(StringDB)) throw new NotFoundException('String não existe!');
        return res.status(HttpStatus.OK).json(StringDB);
        
    }

    // Retorna Lista de StringDBs 
    @Get()
    async getAllStringDB(@Res() res) {
        const StringDBs = await this.DatabaseService.getAllStrings();
        return res.status(HttpStatus.OK).json(StringDBs);
    }

    // Busca uma StringDB específica usando ID
    @Get(':StringDBID')
    async getStringDB(@Res() res, @Param('StringDBID') StringDBID) {
        const StringDB = await this.DatabaseService.getString(StringDBID);
        if (typeof StringDB === 'undefined') {
            throw new NotFoundException('String não existe!');
        }

        return res.status(HttpStatus.OK).json(StringDB);
    }
}