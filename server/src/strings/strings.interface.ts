import { Document } from 'mongoose';

export interface Strings extends Document {
    readonly page: string;
    readonly content:[{
        readonly id:string;
        readonly value:string;
    }]
}



