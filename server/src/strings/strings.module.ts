import { Module } from '@nestjs/common';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { StringSchema } from './strings.schema'
import { StringsController } from './strings.controller';
import { StringsService } from './strings.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'String', schema: StringSchema }]),
  ],
  controllers: [StringsController],
  providers: [StringsService]
})

export class StringsModule { }
