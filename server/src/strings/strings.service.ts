import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Strings } from './strings.interface';
import { StringsDTO } from './strings.dto'
@Injectable()
export class StringsService {

    constructor(@InjectModel('String') private readonly AEModel: Model<Strings>) { }
    // pega todas as Strings
    async getAllStrings(): Promise<Strings[]> {
        const stringsAE = await this.AEModel.find().exec();
        return stringsAE;
    }
    
    // pega String especifica
    async getString(StringID): Promise<Strings> {
        try{
                const stringsAE = await this.AEModel.findById(StringID).exec();
                return stringsAE;
        }catch(err){
    console.log(err);
    }
    }

    // salva nova String --Funcional
    // async addString(AEsDTO: StringsDTO): Promise<Strings> {
    //     const newString = await this.AEModel(AEsDTO);
    //     return newString.save();
    // }

    // Edita String existente --Funcional
    async updateString(StringID, AEsDTO: StringsDTO): Promise<Strings> {
        try{
            const updatedString = await this.AEModel
            .findByIdAndUpdate(StringID, AEsDTO, { new: true });    
            return updatedString;        
        }catch(err){
        console.log(err);
        }
    }

    // deleta String especifica --Funcional
    // async deleteString(StringID): Promise<any> {
    //     const deletedString = await this.AEModel.findByIdAndRemove(StringID);
    //     return deletedString;
    // }
}