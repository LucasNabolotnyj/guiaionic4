import * as mongoose from 'mongoose';
import * as argon2 from 'argon2';

export const UsuarioSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    senha: {
        type: String,
        required: true
    }
});

UsuarioSchema.pre('save',async function(next){

    let usuario = this;
    if(!usuario.isModified('senha')) return next();
    try {
        const hash =  await argon2.hash(usuario.senha,{type: argon2.argon2id,
          timeCost:8,
          memoryCost:512,
          hashLength:50,
          parallelism:8});
        usuario.senha = hash;
            next();
      } catch (err) {
        next(err);
      }
    })

UsuarioSchema.methods.checaSenha =  async function(attempt, callback){
    let usuario = this;
    try {
        if ( await argon2.verify(usuario.senha, attempt)) {
          // senha confere
          callback(null, true);
        } else {
          // senha nao confere
          callback(null, false);
        }
      } catch (err) {
        // falha interna
        return callback(err);
      } 
    

};