import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { CriaUsuarioDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
// import { AuthGuard } from 'src/auth/guards/auth.guard';

@Controller('usuarios')
export class UsersController {

    constructor(private usersService: UsersService) {

    }

    @Post() 
    async criar(@Body() criaUsuarioDto: CriaUsuarioDto) {
        return await this.usersService.create(criaUsuarioDto);
    }

    // Essa rota requer sucesso na verificação do Web Token (JWT)
    @Get('/test')
    @UseGuards(AuthGuard('jwt'))
    testAuthRoute(){

        return {
            message: 'FOI!'
        }
    }

}