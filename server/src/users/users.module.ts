import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule} from '@nestjs/passport';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UsuarioSchema } from './user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'usuarios', schema: UsuarioSchema}]),
    PassportModule.register({ defaultStrategy: 'jwt', session: false })
  ],
  exports: [UsersService],
  controllers: [UsersController],
  providers: [UsersService]
})
export class UsersModule {}
