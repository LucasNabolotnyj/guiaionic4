import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Usuario } from './user.interface';
import { CriaUsuarioDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {

  constructor(@InjectModel('usuarios') private userModel: Model<Usuario>) {}

  async create(criaUsuarioDto: CriaUsuarioDto) {

    let usuarioCriado = new this.userModel(criaUsuarioDto);
    return await usuarioCriado.save();

  }

  async findOneByEmail(email): Model<Usuario> {

    return await this.userModel.findOne({email: email});

  }

    // deleta usuario especifico --Funcional
    // async deleteUsuario(StringID): Promise<any> {
    //     const usuarioDeletado = await this.userModel.findByIdAndRemove(StringID);
    //     return usuarioDeletado;
    // }
}